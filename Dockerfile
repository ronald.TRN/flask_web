FROM ubuntu:18.10

RUN apt-get update
RUN apt-get install -y python3 python3-dev python3-pip nginx
RUN pip3 install uwsgi
RUN pip3 install flask

COPY ./ ./app
WORKDIR ./app

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]
