#flask_docker

## Build docker image

docker build -t flask_web:latest .

## Run docker image

docker run -d -p 5000:5000 flask_web

## Access app

localhost:5000/

